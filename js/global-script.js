$(document).ready(function () {
    // AOS.init({
    //     duration: 1100
    // });
    $(function () {
        $('.wcml-cs-item-toggle').click(function (event) {
            $('.wcml-cs-submenu').slideToggle('fast');
        });
        $(document).click(function (event) {
            if ($(event.target).closest('.wcml-cs-active-currency').length == 0) {
                $('.wcml-cs-submenu').hide();
            }
        });
    });

    var main_slider = new Swiper('.main-slider', {
        slidesPerView: 1,
        effect: 'fade',
        navigation: {
            nextEl: '.swiper-btn-next',
            prevEl: '.swiper-btn-prev'
        },
        // lazy: true,
        loop: true,
        breakpoints: {
            767: {
                slidesPerView: 1
            }
        }
    });

    var reviews_slider = new Swiper('.swiper-reviews', {
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            992: {
                slidesPerView: 2
            },
            767: {
                slidesPerView: 1
            }
        }
    });

    var rooms_slider = new Swiper('.rooms-block', {
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            1025: {
                slidesPerView: 3,
                spaceBetween: 15
            },
            992: {
                slidesPerView: 'auto',
                spaceBetween: 15
            }
        }
    });

    var excursions_block = new Swiper('.excursions-block', {
        slidesPerView: 4,
        breakpoints: {
            1025: {
                slidesPerView: 3,
                spaceBetween: 15
            },
            992: {
                slidesPerView: 'auto',
                spaceBetween: 15
            }
        }
    });

    var swiper_gallery = new Swiper('.swiper-gallery', {
        slidesPerView: 3,
        loop: true,
        navigation: {
            nextEl: '.swiper-btn-next',
            prevEl: '.swiper-btn-prev'
        },
        breakpoints: {
            767: {
                slidesPerView: 1
            }
        }
    });

    $('.popup-modal').magnificPopup({
        type: 'inline',
        gallery: {
            enabled: true
        }
    });

    $('.item-comment').on('mouseenter', function () {
        var height = $(this).find('.text-wrap div').outerHeight();
        if (height > 150) {
            $(this).find('.text-wrap').css('height', height);
        }
    });
    $('.item-comment').on('mouseleave', function () {
        $(this).find('.text-wrap').css('height', '150px');
    });
    $('.features-item .info-wrap').on('mouseenter', function () {
        var height = $(this).find('div').outerHeight();
        if (height > 80) $(this).css('height', height)
    });
    $('.features-item .info-wrap').on('mouseleave', function () {
        $(this).css('height', '50px');
    });


    $('.datepicker').datepicker({
        autoclose: true
    });

    $(".select2").select2();

    $('.header-toggle').on('click', function () {
        $('.header-nav ').toggleClass('open');
        $('header').toggleClass('nav-open');
    });






});
